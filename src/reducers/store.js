import {createStore} from "redux";
import { combined } from "./combine";

export const store = createStore(combined)
